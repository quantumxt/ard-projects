#include <Adafruit_DotStar.h>

#include <SPI.h>
#define NUMPIXELS 30 // Number of LEDs in strip

// Here's how to control the LEDs from any two pins:
#define DATAPIN    4
#define CLOCKPIN   5
Adafruit_DotStar strip = Adafruit_DotStar(
                           NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);
// The last parameter is optional -- this is the color data order of the
// DotStar strip, which has changed over time in different production runs.
// Your code just uses R,G,B colors, the library then reassigns as needed.
// Default is DOTSTAR_BRG, so change this if you have an earlier strip.

// Hardware SPI is a little faster, but must be wired to specific pins
// (Arduino Uno = pin 11 for data, 13 for clock, other boards are different).
//Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DOTSTAR_BRG);

void setup() {
  Serial.begin(115200);

#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin(); // Initialize pins for output
  strip.show();  // Turn all LEDs off ASAP
}

uint32_t color = 0x200000;      // Inital color (starts red)

void loop() {
  for (int j = 0; j < 3; j++) {
    //Loop the diff colors, RGB
    for (int i = 0; i < strip.numPixels(); i++) {
      //Shift by 2 Bits for another color
      strip.setPixelColor(i, color >> j * 8);
      strip.setPixelColor(i + 1, color >> j * 8);
      strip.show();                     // Refresh strip
      //Serial.println((color >> j * 8), HEX);  //Debug
      delay(50);
      clearLED(i);
    }
  }
}

void clearLED(int c) {
  //Turn off LED that are on
  for (int i = 0; i < 2; i++) {
    strip.setPixelColor(c + i, 0); //Off LED
  }
}

