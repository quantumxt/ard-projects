/*
Arduino Flex sensor tutorial
http://cyaninfinite.com/tutorials/bending-with-flex-sensors/
Visit us for more tutorials and projects!
*/

int flexSensorPin = A0; //analog pin 0
int ledPin = 9;
 
void setup(){
  Serial.begin(9600);
  pinMode(ledPin,OUTPUT);
}
 
void loop(){
  int flexSensorReading = analogRead(flexSensorPin); 
 
  Serial.println(flexSensorReading);
 
  //Using map(), I convert the readings to a larger range to determine brightness of LED
  int val = map(flexSensorReading, 460, 540, 0, 255);
  //Serial.println(val);
  analogWrite(ledPin,val);
  //delay(250); //delay
}