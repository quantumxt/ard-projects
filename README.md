Arduino Projects
================

These are the source code used in my arduino projects.
View the various Arduino projects at http://cyaninfinite.com/projects

## Downloading the repo
To download the entire repo, either click on the <i>Clone or Download button</i>, followed by <i>Download Zip</i>, or you can <i>git clone</i> this repo:
```
git clone https://github.com/1487quantum/ard-projects.git
```
## Downloading a single folder
To export only a single directory/folder, use the <i>svn</i> command. (Checkout on how to install svn here: http://ourcodeworld.com/articles/read/123/how-to-download-a-single-folder-of-a-github-repository)
```
svn export <url>
```
For example, to export the <i>led_light_chaser</i> folder, you can do so by running this line:
```
svn export https://github.com/1487quantum/ard-projects/tree/master/led_light_chaser
```

OPEN SOURCE LICENSE
===================

These files are distributed under the GNU GPL Open Source License.
Further information can be found in the LICENSE file.
NO WARRANTY is provided with these files, since they are released under the GNU GPL.
